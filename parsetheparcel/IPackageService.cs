﻿using System.ServiceModel;
using System.Data;

namespace parsetheparcel
{
    [ServiceContract]
    public interface IPackageService
    {
        [OperationContract]
        DataTable ParcePackage(string wsclient, float length, float breadth, float height, float weight, string unit, bool isTest);
    }
}
