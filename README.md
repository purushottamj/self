# README #

Instructions to use the solution

### Parse the Parcel ###

* Test Parse the Parcel Service to get Parcel Type and Cost
* Version 1.0

### Test the solution ###

* Open the solutions in Visual Studio 2015
* Database Server - Localhost and Database Name testing
* Create Database with the same name and Execute the script.sql file 
* Run the project in Visual Studio (Press F5) and get result (Modify the values in Program.cs to get diff results)