﻿using System;

namespace packagecostchecker
{
    class Program
    {
        static void Main(string[] args)
        {
            ParsePackage.PackageServiceClient client = new ParsePackage.PackageServiceClient();
            string wsclient = "388AE3F6-4FEA-4ABC-9CCA-6028C67632A8"; //Basic security to control access
            float len = 210;
            float bre = 320;
            float hei = 100;
            float wei = 20;
            string unit = "mm"; //in case unit is diff.
            bool istest = false; //for test purpose (TBI)

            Console.WriteLine("---------------------------------------");

            //Call method
            try
            {
                var dt = client.ParcePackage(wsclient, len, bre, hei, wei, unit, istest);

                if (dt.Rows.Count > 0)
                {
                    if (dt.Rows[0]["ResponseCode"].ToString() == "0")
                    {
                        Console.WriteLine("ResponseCode: ".PadRight(10) + dt.Rows[0]["ResponseCode"].ToString());
                        Console.WriteLine("ResponseTest: ".PadRight(10) + dt.Rows[0]["ResponseText"].ToString());
                        Console.WriteLine("PackageID: ".PadRight(10) + dt.Rows[0]["PackageID"].ToString());
                        Console.WriteLine("PackageType: ".PadRight(10) + dt.Rows[0]["PackageType"].ToString());
                        Console.WriteLine("PackageCost: ".PadRight(10) + dt.Rows[0]["PackageCost"].ToString());
                    }
                    else
                    {
                        Console.WriteLine("ResponseCode: ".PadRight(10) + dt.Rows[0]["ResponseCode"].ToString());
                        Console.WriteLine("ResponseTest: ".PadRight(10) + dt.Rows[0]["ResponseText"].ToString());
                    }
                }
                else
                {
                    Console.WriteLine("No response returned!");
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());
            }
            Console.WriteLine("---------------------------------------");
            Console.ReadKey();
        }
    }
}
