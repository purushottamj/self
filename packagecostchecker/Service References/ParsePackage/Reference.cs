﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace packagecostchecker.ParsePackage {
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="ParsePackage.IPackageService")]
    public interface IPackageService {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IPackageService/ParcePackage", ReplyAction="http://tempuri.org/IPackageService/ParcePackageResponse")]
        System.Data.DataTable ParcePackage(string wsclient, float length, float breadth, float height, float weight, string unit, bool isTest);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IPackageService/ParcePackage", ReplyAction="http://tempuri.org/IPackageService/ParcePackageResponse")]
        System.Threading.Tasks.Task<System.Data.DataTable> ParcePackageAsync(string wsclient, float length, float breadth, float height, float weight, string unit, bool isTest);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface IPackageServiceChannel : packagecostchecker.ParsePackage.IPackageService, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class PackageServiceClient : System.ServiceModel.ClientBase<packagecostchecker.ParsePackage.IPackageService>, packagecostchecker.ParsePackage.IPackageService {
        
        public PackageServiceClient() {
        }
        
        public PackageServiceClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public PackageServiceClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public PackageServiceClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public PackageServiceClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public System.Data.DataTable ParcePackage(string wsclient, float length, float breadth, float height, float weight, string unit, bool isTest) {
            return base.Channel.ParcePackage(wsclient, length, breadth, height, weight, unit, isTest);
        }
        
        public System.Threading.Tasks.Task<System.Data.DataTable> ParcePackageAsync(string wsclient, float length, float breadth, float height, float weight, string unit, bool isTest) {
            return base.Channel.ParcePackageAsync(wsclient, length, breadth, height, weight, unit, isTest);
        }
    }
}
